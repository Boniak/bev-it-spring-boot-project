package org.common.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "account")
public class Account {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	@Column(name = "username")
	String username;
	@Column(name = "password")
	String password;
	@Column(name = "ruolo")
	char ruolo;
	@OneToOne
	@JoinColumn(name = "id_contatto", referencedColumnName = "id")
	Contatto idContatto;
	@Column(name = "enable_account")
	boolean enableAccount;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public char getRuolo() {
		return ruolo;
	}
	public void setRuolo(char ruolo) {
		this.ruolo = ruolo;
	}
	public Contatto getIdContatto() {
		return idContatto;
	}
	public void setIdContatto(Contatto idContatto) {
		this.idContatto = idContatto;
	}
	public boolean isEnableAccount() {
		return enableAccount;
	}
	public void setEnableAccount(boolean enableAccount) {
		this.enableAccount = enableAccount;
	}
	
	public Account() {
		super();
	}
	public Account(int id, String username, String password, char ruolo, Contatto idContatto,boolean enableAccount) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.ruolo = ruolo;
		this.idContatto = idContatto;
		this.enableAccount = enableAccount;
	}
	
	
	@Override
	public String toString() {
		return "Account [id=" + id + ", username=" + username + ", password=" + password + ", ruolo=" + ruolo
				+ ", idContatto=" + idContatto + ", enableAccount=" + enableAccount +"]";
	}
	
}
