package org.common.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "carrello")
public class Carrello {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	@ManyToOne
	@JoinColumn(name = "id_cliente", referencedColumnName = "id")
	Account idAccount;
	@ManyToOne
	@JoinColumn(name = "id_prodotto", referencedColumnName = "id")
	Prodotto idProdotto;
	@Column(name = "quantita_carrello")
	int quantita;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Account getIdAccount() {
		return idAccount;
	}
	public void setIdAccount(Account idAccount) {
		this.idAccount = idAccount;
	}
	public Prodotto getIdProdotto() {
		return idProdotto;
	}
	public void setIdProdotto(Prodotto idProdotto) {
		this.idProdotto = idProdotto;
	}
	public int getQuantita() {
		return quantita;
	}
	public void setQuantita(int quantita) {
		this.quantita = quantita;
	}
	
	
	public Carrello() {
		super();
	}
	public Carrello(int id, Account idAccount, Prodotto idProdotto, int quantita) {
		super();
		this.id = id;
		this.idAccount = idAccount;
		this.idProdotto = idProdotto;
		this.quantita = quantita;
	}

	
	public void addQuantitaByOne() {
		this.quantita++;
	}
	public void removeQuantitaByOne() {
		this.quantita--;
	}
	
	
	@Override
	public String toString() {
		return "Carrello [id=" + id + ", idAccount=" + idAccount + ", idProdotto=" + idProdotto + ", quantita="
				+ quantita + "]";
	}
	
}
