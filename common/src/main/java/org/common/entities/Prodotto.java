package org.common.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "prodotto")
public class Prodotto {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	@Column(name = "descrizione")
	String descrizione;
	@Column(name = "bevanda_alcolica")
	boolean bevandaAlcolica;
	@Column(name = "prezzo")
	double prezzo;
	@Column(name = "aliquota_iva")
	int aliquotaIva;
	@Column(name = "enable_prodotto")
	boolean enableProdotto;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	public boolean isBevandaAlcolica() {
		return bevandaAlcolica;
	}
	public void setBevandaAlcolica(boolean bevandaAlcolica) {
		this.bevandaAlcolica = bevandaAlcolica;
	}
	public double getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(double prezzo) {
		this.prezzo = prezzo;
	}
	public int getAliquotaIva() {
		return aliquotaIva;
	}
	public void setAliquotaIva(int aliquotaIva) {
		this.aliquotaIva = aliquotaIva;
	}
	public boolean isEnableProdotto() {
		return enableProdotto;
	}
	public void setEnableProdotto(boolean enableProdotto) {
		this.enableProdotto = enableProdotto;
	}
	
	
	public Prodotto() {
		super();
	}
	public Prodotto(int id, String descrizione, boolean bevandaAlcolica, double prezzo, int aliquotaIva,
			boolean enableProdotto) {
		super();
		this.id = id;
		this.descrizione = descrizione;
		this.bevandaAlcolica = bevandaAlcolica;
		this.prezzo = prezzo;
		this.aliquotaIva = aliquotaIva;
		this.enableProdotto = enableProdotto;
	}
	
	
	@Override
	public String toString() {
		return "Prodotto [id=" + id + ", descrizione=" + descrizione + ", bevandaAlcolica=" + bevandaAlcolica
				+ ", prezzo=" + prezzo + ", aliquotaIva=" + aliquotaIva + ", enableProdotto=" + enableProdotto + "]";
	}

}
