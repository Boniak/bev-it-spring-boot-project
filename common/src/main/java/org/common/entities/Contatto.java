package org.common.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "contatto")
public class Contatto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	@Column(name = "nome")
	String nome;
	@Column(name = "cognome")
	String cognome;
	@Column(name = "codice_fiscale")
	String codiceFiscale;
	@Column(name = "email")
	String email;
	@Column(name = "telefono")
	String telefono;
	@OneToOne
	@JoinColumn(name = "id_indirizzo", referencedColumnName = "id")
	Indirizzo idIndirizzo;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getCodiceFiscale() {
		return codiceFiscale;
	}
	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public Indirizzo getIdIndirizzo() {
		return idIndirizzo;
	}
	public void setIdIndirizzo(Indirizzo idIndirizzo) {
		this.idIndirizzo = idIndirizzo;
	}
	
	
	public Contatto() {
		super();
	}
	public Contatto(int id, String nome, String cognome, String codiceFiscale, String email, String telefono,
			Indirizzo idIndirizzo) {
		super();
		this.id = id;
		this.nome = nome;
		this.cognome = cognome;
		this.codiceFiscale = codiceFiscale;
		this.email = email;
		this.telefono = telefono;
		this.idIndirizzo = idIndirizzo;
	}
	
	
	@Override
	public String toString() {
		return "Contatto [id=" + id + ", nome=" + nome + ", cognome=" + cognome + ", codiceFiscale=" + codiceFiscale
				+ ", email=" + email + ", telefono=" + telefono + ", idIndirizzo=" + idIndirizzo + "]";
	}
	
}
