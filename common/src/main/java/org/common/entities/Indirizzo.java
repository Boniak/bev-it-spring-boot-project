package org.common.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "indirizzo")
public class Indirizzo {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	@Column(name = "stato")
	String stato;
	@Column(name = "citta")
	String citta;
	@Column(name = "provincia")
	String provincia;
	@Column(name = "cap")
	String cap;
	@Column(name = "via")
	String via;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getStato() {
		return stato;
	}
	public void setStato(String stato) {
		this.stato = stato;
	}
	public String getCitta() {
		return citta;
	}
	public void setCitta(String citta) {
		this.citta = citta;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getCap() {
		return cap;
	}
	public void setCap(String cap) {
		this.cap = cap;
	}
	public String getVia() {
		return via;
	}
	public void setVia(String via) {
		this.via = via;
	}
	
	
	public Indirizzo() {
		super();
	}
	public Indirizzo(int id, String stato, String citta, String provincia, String cap, String via) {
		super();
		this.id = id;
		this.stato = stato;
		this.citta = citta;
		this.provincia = provincia;
		this.cap = cap;
		this.via = via;
	}
	
	
	@Override
	public String toString() {
		return "Indirizzo [id=" + id + ", stato=" + stato + ", citta=" + citta + ", provincia=" + provincia
				+ ", cap=" + cap + ", via=" + via + "]";
	}
	
}
