package org.common.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "dettaglio_fattura")
public class DettaglioFattura {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	@ManyToOne
	@JoinColumn(name = "id_fattura", referencedColumnName = "id")
	Fattura idFattura;
	@ManyToOne
	@JoinColumn(name = "id_prodotto", referencedColumnName = "id")
	Prodotto idProdotto;
	@Column(name = "quantita")
	int quantita;
	@Column(name = "prezzo_unitario")
	double prezzoUnitario;	
	@Column(name = "aliquota_iva")
	int aliquotaIva;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Fattura getIdFattura() {
		return idFattura;
	}
	public void setIdFattura(Fattura idFattura) {
		this.idFattura = idFattura;
	}
	public Prodotto getIdProdotto() {
		return idProdotto;
	}
	public void setIdProdotto(Prodotto idProdotto) {
		this.idProdotto = idProdotto;
	}
	public int getQuantita() {
		return quantita;
	}
	public void setQuantita(int quantita) {
		this.quantita = quantita;
	}
	public double getPrezzoUnitario() {
		return prezzoUnitario;
	}
	public void setPrezzoUnitario(double prezzoUnitario) {
		this.prezzoUnitario = prezzoUnitario;
	}
	public int getAliquotaIva() {
		return aliquotaIva;
	}
	public void setAliquotaIva(int aliquotaIva) {
		this.aliquotaIva = aliquotaIva;
	}
	
	
	public DettaglioFattura() {
		super();
	}
	public DettaglioFattura(int id, Fattura idFattura, Prodotto idProdotto, int quantita, double prezzoUnitario,
			int aliquotaIva) {
		super();
		this.id = id;
		this.idFattura = idFattura;
		this.idProdotto = idProdotto;
		this.quantita = quantita;
		this.prezzoUnitario = prezzoUnitario;
		this.aliquotaIva = aliquotaIva;
	}
	
	
	@Override
	public String toString() {
		return "DettaglioFattura [id=" + id + ", idFattura=" + idFattura + ", idProdotto=" + idProdotto + ", quantita="
				+ quantita + ", prezzoUnitario=" + prezzoUnitario + ", aliquotaIva=" + aliquotaIva + "]";
	}
	
}
