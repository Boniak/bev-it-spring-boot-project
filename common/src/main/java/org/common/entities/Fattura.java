package org.common.entities;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "fattura")
public class Fattura {	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	@Column(name = "numero")
	int numero;
	@Column(name = "totale")
	double totale;
	@Column(name = "data")
	Date data;
	@ManyToOne
	@JoinColumn(name = "id_account")
	Account idAccount;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}	
	public double getTotale() {
		return totale;
	}
	public void setTotale(double totale) {
		this.totale = totale;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public Account getIdAccount() {
		return idAccount;
	}
	public void setIdAccount(Account idAccount) {
		this.idAccount = idAccount;
	}

	
	public Fattura() {
		super();
	}
	public Fattura(int id, int numero, double totale, Date data, Account idAccount) {
		super();
		this.id = id;
		this.numero = numero;
		this.totale = totale;
		this.data = data;
		this.idAccount = idAccount;
	}
	
	
	@Override
	public String toString() {
		return "Fattura [id=" + id + ", numero=" + numero + ", totale=" + totale + ", data=" + data + ", idAccount="
				+ idAccount + "]";
	}
	
}
