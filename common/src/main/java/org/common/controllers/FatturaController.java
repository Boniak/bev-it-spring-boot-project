package org.common.controllers;

import java.util.List;
import java.util.Optional;

import org.common.entities.Fattura;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface FatturaController {

	@GetMapping("/list")
	List<Fattura> getListFattura();
	
	@GetMapping("/{id}")
	Optional<Fattura> getFatturaById(@PathVariable(name = "id") int id);
	
	@PostMapping("/insert")
	Fattura insertFattura(@RequestBody Fattura fattura);
	
	@PutMapping("/update")
	Fattura updateFattura(@RequestBody Fattura fattura);
	
	@DeleteMapping("/delete")
	void deleteFattura(@RequestBody Fattura fattura);
	
	@GetMapping("/list/{idAccount}")
	List<Fattura> getListFatturaByAccountId(@PathVariable(name = "idAccount")int idAccount);
	
	@GetMapping("/max-numero-fattura")
	int selectMaxNumeroFattura();
	
}
