package org.common.controllers;

import java.util.List;
import java.util.Optional;

import org.common.entities.Contatto;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface ContattoController {

	@GetMapping("/list")
	List<Contatto> getListContatto();
	
	@GetMapping("/{id}")
	Optional<Contatto> getContattoById(@PathVariable(name = "id") int id);
	
	@PostMapping("/insert")
	Contatto insertContatto(@RequestBody Contatto contatto);
	
	@PutMapping("/update")
	Contatto updateContatto(@RequestBody Contatto contatto);
	
	@DeleteMapping("/delete")
	void deleteContatto(@RequestBody Contatto contatto);
	
}
