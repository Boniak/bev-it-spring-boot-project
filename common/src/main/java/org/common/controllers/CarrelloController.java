package org.common.controllers;

import java.util.List;
import java.util.Optional;

import org.common.entities.Carrello;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

public interface CarrelloController {

	@GetMapping("/list")
	List<Carrello> getListCarrello();
	
	@GetMapping("/{id}")
	Optional<Carrello> getCarrelloById(@PathVariable(name = "id") int id);
	
	@PostMapping("/insert")
	Carrello insertCarrello(@RequestBody Carrello carrello);
	
	@PutMapping("/update")
	Carrello updateCarrello(@RequestBody Carrello carrello);
	
	@DeleteMapping("/delete")
	void deleteCarrello(@RequestBody Carrello carrello);
	
	@GetMapping("/check-account-prodotto")
	Carrello checkProdottoInCarrello(@RequestParam int idAccount, @RequestParam int idProdotto);
	
	@GetMapping("/list/{idAccount}")
	List<Carrello> getListCarrelloByAccountId(@PathVariable(name = "idAccount") int idAccount);
	
	@DeleteMapping("/carrello/{idAccount}")
	void deleteCarrelloByUtenteId(@PathVariable(name = "idAccount") int idAccount);
	
	@GetMapping("/quantita-carrello/{idAccount}")
	int getQuantitaProdottiInCarrelloByAccountId(@PathVariable(name = "idAccount") int idAccount);
	
}
