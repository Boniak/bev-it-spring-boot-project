package org.common.controllers;

import java.util.List;
import java.util.Optional;

import org.common.entities.Account;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

public interface AccountController {

	@GetMapping("/list")
	List<Account> getListAccount();
	
	@GetMapping("/{id}")
	Optional<Account> getAccountById(@PathVariable(name = "id") int id);
	
	@PostMapping("/insert")
	Account insertAccount(@RequestBody Account account);
	
	@PutMapping("/update")
	Account updateAccount(@RequestBody Account account);
	
	@DeleteMapping("/delete")
	void deleteAccount(@RequestBody Account account);
	
	@GetMapping("/login-check")
	Optional<Account> checkAccountByUsernameAndPassword(@RequestParam String username, @RequestParam String password);
	
	@GetMapping("/list/{ruolo}")
	List<Account> getListAccountByRuolo(@PathVariable(name = "ruolo")char ruolo);
	
	@GetMapping("/username/check")
	boolean checkIfUsernameIsPresent(@RequestParam String username);
	
	@PutMapping("/disable/{idAccount}")
	void disableAccount(@PathVariable(name = "idAccount") int idAccount);
	
	@GetMapping("/list/nome-cognome")
	List<Account> getListAccountByNomeAndCognome(@RequestParam String nome, @RequestParam String cognome);
	
	@GetMapping("/nome/{idAccount}")
	String getNomeByAccountId(@PathVariable(name = "idAccount") int idAccount);
	
	@GetMapping("/cognome/{idAccount}")
	String getCognomeByAccountId(@PathVariable(name = "idAccount") int idAccount);
	
}	
