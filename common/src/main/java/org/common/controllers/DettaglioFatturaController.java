package org.common.controllers;

import java.util.List;
import java.util.Optional;

import org.common.entities.DettaglioFattura;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface DettaglioFatturaController {

	@GetMapping("/list")
	List<DettaglioFattura> getListDettaglioFattura();
	
	@GetMapping("/{id}")
	Optional<DettaglioFattura> getDettaglioFatturaById(@PathVariable(name = "id") int id);
	
	@PostMapping("/insert")
	DettaglioFattura insertDettaglioFattura(@RequestBody DettaglioFattura dettaglioFattura);
	
	@PutMapping("/update")
	DettaglioFattura updateDettaglioFattura(@RequestBody DettaglioFattura dettaglioFattura);
	
	@DeleteMapping("/delete")
	void deleteDettaglioFattura(@RequestBody DettaglioFattura dettaglioFattura);
	
	@GetMapping("/lista-dettagli/{idFattura}")
	List<DettaglioFattura> getListDettagliFatturaByFatturaId(@PathVariable(name = "idFattura") int idFattura);
	
}
