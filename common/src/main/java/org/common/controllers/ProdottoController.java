package org.common.controllers;

import java.util.List;
import java.util.Optional;

import org.common.entities.Prodotto;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface ProdottoController {

	@GetMapping("/list")
	List<Prodotto> getListProdotto();
	
	@GetMapping("/{id}")
	Optional<Prodotto> getProdottoById(@PathVariable(name = "id") int id);
	
	@PostMapping("/insert")
	Prodotto insertProdotto(@RequestBody Prodotto indirizzo);
	
	@PutMapping("/update")
	Prodotto updateProdotto(@RequestBody Prodotto indirizzo);
	
	@PutMapping("/disable/{idProdotto}")
	void disableProdotto(@PathVariable(name = "idProdotto")int idProdotto);
	
	@DeleteMapping("/delete")
	void deleteProdotto(@RequestBody Prodotto indirizzo);
	
}
