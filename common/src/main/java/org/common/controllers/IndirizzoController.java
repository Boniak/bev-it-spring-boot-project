package org.common.controllers;

import java.util.List;
import java.util.Optional;

import org.common.entities.Indirizzo;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface IndirizzoController {

	@GetMapping("/list")
	List<Indirizzo> getListIndirizzo();
	
	@GetMapping("/{id}")
	Optional<Indirizzo> getIndirizzoById(@PathVariable(name = "id") int id);
	
	@PostMapping("/insert")
	Indirizzo insertIndirizzo(@RequestBody Indirizzo indirizzo);
	
	@PutMapping("/update")
	Indirizzo updateIndirizzo(@RequestBody Indirizzo indirizzo);
	
	@DeleteMapping("/delete")
	void deleteIndirizzo(@RequestBody Indirizzo indirizzo);
	
}
