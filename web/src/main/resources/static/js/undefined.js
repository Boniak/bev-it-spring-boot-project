/**
 * 
 */

// funzione per mostrare la password nella pagina di login
function showPassword() {
  var psw = document.getElementById("psw");
  if (psw.type === "password") {
    psw.type = "text";
  } else {
    psw.type = "password";
  }
}
