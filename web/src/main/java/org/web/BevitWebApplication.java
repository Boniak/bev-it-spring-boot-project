package org.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * author Luca Boni
 *
 */
@SpringBootApplication
@EnableFeignClients(basePackages = "org.web.webservices")
public class BevitWebApplication implements CommandLineRunner{
	
    public static void main(String[] args) {
		SpringApplication.run(BevitWebApplication.class, args);
		Logger log = LoggerFactory.getLogger(BevitWebApplication.class);
		log.info("Funge");
	}

	@Override
	public void run(String... args) throws Exception {
		
	}
}