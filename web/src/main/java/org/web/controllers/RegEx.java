package org.web.controllers;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegEx {
	
	Pattern pattern;
	Matcher matcher;
	
	// ---------------------- REGEX ----------------------------
	
	private static String REGEX_CODICE_FISCALE= "/^(?:[A-Z][AEIOU][AEIOUX]|[B-DF-HJ-NP-TV-Z]{2}[A-Z]){2}(?:[\\dLMNP-V]{2}"
			+ "(?:[A-EHLMPR-T](?:[04LQ][1-9MNP-V]|[15MR][\\dLMNP-V]|[26NS][0-8LMNP-U])|[DHPS][37PT][0L]|[ACELMRT]"
			+ "[37PT][01LM]|[AC-EHLMPR-T][26NS][9V])|(?:[02468LNQSU][048LQU]|[13579MPRTV][26NS])B[26NS][9V])(?:[A-MZ]"
			+ "[1-9MNP-V][\\dLMNP-V]{2}|[A-M][0L](?:[1-9MNP-V][\\dLMNP-V]|[0L][1-9MNP-V]))[A-Z]$/i";
	
	private static String REGEX_EMAIL = "(?:[A-Za-z0-9&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9&"
			+ "'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|"
			+ "\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)"
			+ "+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]"
			+ "))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\\x01"
			+ "-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
	
	
	// --------------------- METODI ----------------------------
	
	public boolean checkCodiceFiscale(String codiceFiscale) {	
		pattern = Pattern.compile(REGEX_CODICE_FISCALE);
		matcher = pattern.matcher(codiceFiscale);
		return matcher.find();
	}
	
	public boolean checkEmail(String email) {	
		pattern = Pattern.compile(REGEX_EMAIL);
		matcher = pattern.matcher(email);
		return matcher.find();
	}
	
	
}


























