package org.web.controllers.admin;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.common.entities.Account;
import org.common.entities.Contatto;
import org.common.entities.DettaglioFattura;
import org.common.entities.Fattura;
import org.common.entities.Indirizzo;
import org.common.entities.Prodotto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.web.webservices.AccountWebService;
import org.web.webservices.ContattoWebService;
import org.web.webservices.DettaglioFatturaWebService;
import org.web.webservices.FatturaWebService;
import org.web.webservices.IndirizzoWebService;
import org.web.webservices.ProdottoWebService;

@Controller
public class AdminController {

	@Autowired
	ProdottoWebService prodottoWebService;

	@Autowired
	AccountWebService accountWebService;

	@Autowired
	ContattoWebService contattoWebService;

	@Autowired
	IndirizzoWebService indirizzoWebService;

	@Autowired
	FatturaWebService fatturaWebService;

	@Autowired
	DettaglioFatturaWebService dettaglioFatturaWebService;


	// -------------------------- HOME ------------------------------

	@RequestMapping("/admin/home")
	String home(HttpServletRequest request) {
		request.setAttribute("currentPage", "home");
		return "admin-role/admin-app";
	}

	// ------------------------ PRODOTTI ----------------------------

	@RequestMapping("/admin/prodotti")
	String prodotti(HttpServletRequest request) {
		// prendo la lista dei prodotti dal DB
		List<Prodotto> listaProdotti = prodottoWebService.getListProdotto();
		request.setAttribute("listaProdotti", listaProdotti);
		request.setAttribute("currentPage", "prodotti");
		return "admin-role/admin-app";
	}

	@RequestMapping("/admin/pagina-inserimento-prodotto")
	String paginaInserimentoProdotto(HttpServletRequest request) {
		request.setAttribute("action", "insert");
		request.setAttribute("currentPage", "prodotto-actions");
		return "admin-role/admin-app";
	}

	@RequestMapping("/admin/inserimento-prodotto")
	String inserimentoProdotto(HttpServletRequest request) {

		String nomeProdotto = request.getParameter("nome");
		boolean bevandaAlcolica = Boolean.valueOf(request.getParameter("gradazione"));
		double prezzo = Double.valueOf(request.getParameter("prezzo").replace(',', '.').replace('€', ' '));
		int aliquotaIva = Integer.valueOf(request.getParameter("aliquota"));

		prodottoWebService.insertProdotto(new Prodotto(0,nomeProdotto,bevandaAlcolica,prezzo,aliquotaIva,true));

		// prendo la lista aggiornata dei prodotti dal DB
		List<Prodotto> listaProdotti = prodottoWebService.getListProdotto();				
		request.setAttribute("listaProdotti", listaProdotti);

		request.setAttribute("currentPage", "prodotti");
		return "admin-role/admin-app";
	}


	@RequestMapping("/admin/pagina-modifica-prodotto")
	String paginaModificaProdotto(HttpServletRequest request) {
		request.setAttribute("action", "update");

		int idProdotto = Integer.valueOf(request.getParameter("idProdotto"));
		Prodotto prodottoDaModificare = prodottoWebService.getProdottoById(idProdotto).get();
		request.setAttribute("prodottoToUpdate", prodottoDaModificare);

		request.setAttribute("currentPage", "prodotto-actions");
		return "admin-role/admin-app";
	}


	@RequestMapping("/admin/modifica-prodotto")
	String modificaProdotto(HttpServletRequest request) {

		int idProdotto = Integer.parseInt(request.getParameter("id"));
		String descrizione = request.getParameter("nome");
		boolean gradazione = Boolean.valueOf(request.getParameter("gradazione"));
		double prezzo = Double.valueOf(request.getParameter("prezzo").replace(',', '.').replace('€', ' '));
		int aliquotaIva = Integer.parseInt(request.getParameter("aliquota"));

		prodottoWebService.updateProdotto(new Prodotto(idProdotto,descrizione,gradazione,prezzo,aliquotaIva,true));

		// prendo la lista aggiornata dei prodotti dal DB
		List<Prodotto> listaProdotti = prodottoWebService.getListProdotto();				
		request.setAttribute("listaProdotti", listaProdotti);

		request.setAttribute("currentPage", "prodotti");
		return "admin-role/admin-app";
	}


	/* non elimino definitivamente il prodotto, lo disabilito impostando il valore del campo 'enable_prodotto' a false.
	 * questo per non creare problemi al campo 'id_prodotto' della tabella 'dettaglio_fattura'.
	 */
	@RequestMapping("/admin/eliminazione-prodotto")
	String eliminaProdotto(HttpServletRequest request) {

		int idProdotto = Integer.valueOf(request.getParameter("idProdotto"));

		prodottoWebService.disableProdotto(idProdotto);

		// prendo la lista aggiornata dei prodotti dal DB
		List<Prodotto> listaProdotti = prodottoWebService.getListProdotto();				
		request.setAttribute("listaProdotti", listaProdotti);

		request.setAttribute("currentPage", "prodotti");
		return "admin-role/admin-app";
	}

	// ------------------------ ORDINI ------------------------------

	@RequestMapping("/admin/ordini")
	String ordini(HttpServletRequest request) {
		List<Fattura> listaOrdini = fatturaWebService.getListFattura();
		request.setAttribute("listaOrdini", listaOrdini);		
		request.setAttribute("currentPage", "ordini");
		return "admin-role/admin-app";
	}

	@RequestMapping("/admin/dettagli-fattura")
	String dettagliFattura(HttpServletRequest request) {
		int idFattura = Integer.parseInt(request.getParameter("idFattura"));
		List<DettaglioFattura> listaDettagliFattura = dettaglioFatturaWebService.getListDettagliFatturaByFatturaId(idFattura);
		double totalPrice = 0.0;
		for (DettaglioFattura dettaglioFattura : listaDettagliFattura) {
			totalPrice += dettaglioFattura.getPrezzoUnitario() * dettaglioFattura.getQuantita();
		}
		request.setAttribute("listaDettagliFattura", listaDettagliFattura);
		request.setAttribute("totalDettagliPrice", totalPrice);

		request.setAttribute("currentPage", "dettagli-fattura");
		return "admin-role/admin-app";
	}

	// ------------------------ UTENTI -------------------------------

	@RequestMapping("/admin/utenti")
	String utenti(HttpServletRequest request) {
		List<Account> listaUtentiUser = accountWebService.getListAccountByRuolo('U');
		List<Account> listaUtentiAdmin = accountWebService.getListAccountByRuolo('A');

		request.setAttribute("listUser", listaUtentiUser);
		request.setAttribute("listAdmin", listaUtentiAdmin);

		request.setAttribute("currentPage", "utenti");
		return "admin-role/admin-app";
	}

	@RequestMapping("/admin/pagina-inserimento-account")
	String paginaInserimentoAccount(HttpServletRequest request) {
		request.setAttribute("currentPage", "inserimento-account");
		return "admin-role/admin-app";
	}

	@RequestMapping("/admin/inserimento-account")
	String inserimentoAccount(HttpServletRequest request) {

		String errorMessage = "";

		// provo ad eseguire la registrazione, se non va a buon fine,
		// reindirizzo alla pagina di registrazione con un messaggio di errore
		try {

			// indirizzo
			String stato = request.getParameter("stato");
			errorMessage = stato.equals("") ? "Stato can not be empty" : errorMessage;

			String citta = request.getParameter("citta");
			errorMessage = citta.equals("") ? "Citta' can not be empty" : errorMessage;

			String provincia = request.getParameter("provincia").toUpperCase();
			errorMessage = provincia.equals("") ? "Provincia can not be empty" : errorMessage;

			String cap = request.getParameter("cap");
			errorMessage = cap.equals("") ? "Cap can not be empty" : errorMessage;

			String via = request.getParameter("via");
			errorMessage = via.equals("") ? "Via can not be empty" : errorMessage;


			// contatto
			String nome = request.getParameter("nome");
			errorMessage = nome.equals("") ? "Nome can not be empty" : errorMessage;

			String cognome = request.getParameter("cognome");
			errorMessage = cognome.equals("") ? "Cognome can not be empty" : errorMessage;

			String email = request.getParameter("email");
			errorMessage = email.equals("") ? "Email can not be empty" : errorMessage;

			String codiceFiscale = request.getParameter("codiceFiscale");			
			errorMessage = codiceFiscale.equals("") ? "Codice Fiscale can not be empty" : errorMessage;

			String numeroDiTel = request.getParameter("numero");
			errorMessage = numeroDiTel.equals("") ? "Numero di Telefono can not be empty" : errorMessage;


			// account
			String username = request.getParameter("username");
			errorMessage = username.equals("") ? "Username can not be empty" : errorMessage;
			String password = request.getParameter("password");
			errorMessage = password.equals("") ? "Password can not be empty" : errorMessage;


			// ----------------------------- CHECKS -----------------------------------

			/**
			 * check per controllare se uno dei parametri inseriti risulta vuoto.
			 * se si, l'utente verrà reindirizzato nuovamente alla pagina di registrazione
			 * con un messaggio di errore specifico valorizzato con il valore di 'errorMessage'.
			 */
			if (!errorMessage.equals("")) {
				// messaggio di errore
				request.setAttribute("msgInsertError", errorMessage);

				request.setAttribute("currentPage", "inserimento-account");
				return "admin-role/admin-app";
			}

			/**
			 * check per controllare se l'username è già presente nel DB
			 * se si, l'utente verrà reindirizzato nuovamente alla pagina di registrazione
			 * con un messaggio di errore valorizzato con il valore di 'errorMessage'.
			 */
			if(accountWebService.checkIfUsernameIsPresent(username)) {
				// messaggio di errore
				errorMessage = "Username already present!";
				request.setAttribute("msgInsertError", errorMessage);

				request.setAttribute("currentPage", "inserimento-account");
				return "admin-role/admin-app";
			}


			// -------------------------- REGISTRATION --------------------------------

			Indirizzo indirizzo = indirizzoWebService
					.insertIndirizzo(new Indirizzo(0, stato, citta, provincia, cap, via));
			Contatto contatto = contattoWebService
					.insertContatto(new Contatto(0, nome, cognome, codiceFiscale, email, numeroDiTel, indirizzo));

			char ruolo = request.getParameter("role").charAt(0);
			accountWebService.insertAccount(new Account(0, username, password, ruolo, contatto, true));

		} catch (Exception e) {
			// messaggio di errore
			errorMessage = "Something went wrong!";
			request.setAttribute("msgErrorRegistration", errorMessage);

			request.setAttribute("currentPage", "inserimento-account");
			return "admin-role/admin-app";
		}

		List<Account> listaUtentiUser = accountWebService.getListAccountByRuolo('U');
		List<Account> listaUtentiAdmin = accountWebService.getListAccountByRuolo('A');

		request.setAttribute("listUser", listaUtentiUser);
		request.setAttribute("listAdmin", listaUtentiAdmin);

		request.setAttribute("currentPage", "utenti");
		return "admin-role/admin-app";
	}

	@RequestMapping("/admin/cerca-utenti")
	String cercaUtenti(HttpServletRequest request) {
		String nome = request.getParameter("nome");
		String cognome = request.getParameter("cognome");

		List<Account> listaUtentiUser = new ArrayList<Account>();
		List<Account> listaUtentiAdmin = new ArrayList<Account>();

		accountWebService.getListAccountByNomeAndCognome(nome, cognome).stream().forEach(account -> {
			if(account.getRuolo() == 'U') {
				listaUtentiUser.add(account);
			} else {
				listaUtentiAdmin.add(account);
			}
		});

		request.setAttribute("listUser", listaUtentiUser);
		request.setAttribute("listAdmin", listaUtentiAdmin);

		request.setAttribute("currentPage", "utenti");
		return "admin-role/admin-app";
	}

	@RequestMapping("/admin/info-utente")
	String infoUtente(HttpServletRequest request) {
		int idAccount = Integer.parseInt(request.getParameter("idAccount"));
		Account accountInfo = accountWebService.getAccountById(idAccount).get();
		request.setAttribute("accountInfo", accountInfo.getIdContatto().getCognome() + " " + accountInfo.getIdContatto().getNome());
		request.setAttribute("isEnable", accountInfo.isEnableAccount());
		
		List<Fattura> lista = fatturaWebService.getListFatturaByAccountId(idAccount);
		request.setAttribute("listFattureCliente", lista);
		request.setAttribute("currentPage", "info-utente");
		return "admin-role/admin-app";
	}	

	// ------------------------ PROFILO -------------------------------

	@RequestMapping("/admin/profilo")
	String profilo(HttpServletRequest request) {
		request.setAttribute("currentPage", "profilo");
		return "admin-role/admin-app";
	}

	@RequestMapping("/admin/elimina-account")
	String eliminaProfilo(HttpServletRequest request) {
		int idAccount = Integer.parseInt(request.getParameter("idAccount"));
		accountWebService.disableAccount(idAccount);
		request.getSession().removeAttribute("accountLog");
		request.getSession().setAttribute("accountRole", "undefined");
		request.setAttribute("currentPage", "home");
		return "undefined-role/undefined-app";
	}

	@RequestMapping("/admin/pagina-modifica-profilo")
	String paginaModificaProfilo(HttpServletRequest request) {	
		request.setAttribute("currentPage", "modifica-profilo");
		return "admin-role/admin-app";
	}

	@RequestMapping("/admin/modifica-profilo")
	String modificaProfilo(HttpServletRequest request) {

		String errorMessage = "";

		int idAccount = Integer.parseInt(request.getParameter("idAccount"));
		String username = request.getParameter("username");
		errorMessage = username.equals("") ? "Username can not be empty" : errorMessage;
		String password = request.getParameter("password");
		errorMessage = password.equals("") ? "Password can not be empty" : errorMessage;

		int idContatto = Integer.parseInt(request.getParameter("idContatto"));
		String nome = request.getParameter("nome");
		errorMessage = nome.equals("") ? "Nome can not be empty" : errorMessage;
		String cognome = request.getParameter("cognome");
		errorMessage = cognome.equals("") ? "Cognome can not be empty" : errorMessage;
		String email = request.getParameter("email");
		errorMessage = email.equals("") ? "Email can not be empty" : errorMessage;
		String codiceFiscale = request.getParameter("codiceFiscale");
		errorMessage = codiceFiscale.equals("") ? "Codice Fiscale can not be empty" : errorMessage;
		String numero = request.getParameter("numero");
		errorMessage = numero.equals("") ? "Numero can not be empty" : errorMessage;

		int idIndirizzo = Integer.parseInt(request.getParameter("idIndirizzo"));
		String stato = request.getParameter("stato");
		errorMessage = stato.equals("") ? "Stato can not be empty" : errorMessage;
		String citta = request.getParameter("citta");
		errorMessage = citta.equals("") ? "Citta can not be empty" : errorMessage;
		String provincia = request.getParameter("provincia");
		errorMessage = provincia.equals("") ? "Provincia can not be empty" : errorMessage;
		String cap = request.getParameter("cap");
		errorMessage = cap.equals("") ? "Cap can not be empty" : errorMessage;
		String via = request.getParameter("via");
		errorMessage = via.equals("") ? "Via can not be empty" : errorMessage;

		if (!errorMessage.equals("")) {
			// messaggio di errore
			request.setAttribute("msgUpdateError", errorMessage);
			request.setAttribute("currentPage", "modifica-profilo");
			return "admin-role/admin-app";
		}

		Indirizzo indirizzo = indirizzoWebService.updateIndirizzo(new Indirizzo(idIndirizzo,stato,citta,provincia,cap,via));
		Contatto contatto = contattoWebService.updateContatto(new Contatto(idContatto,nome,cognome,codiceFiscale,email,numero,indirizzo));
		try {
			Account account = accountWebService.updateAccount(new Account(idAccount,username,password,'A',contatto,true));
			request.getSession().setAttribute("accountLog", account);
			request.setAttribute("currentPage", "profilo");
			return "admin-role/admin-app";
		} catch (Exception e) {
			request.setAttribute("msgUpdateError", "username gia' presente!");
			request.setAttribute("currentPage", "modifica-profilo");
			return "admin-role/admin-app";
		}
	}

}
