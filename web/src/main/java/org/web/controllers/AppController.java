package org.web.controllers;

import javax.servlet.http.HttpServletRequest;

import org.common.entities.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.web.webservices.AccountWebService;
import org.web.webservices.CarrelloWebService;

@Controller
public class AppController {

	@Autowired
	AccountWebService accountWebService;

	@Autowired
	CarrelloWebService carrelloWebService;


	// --------------------------- APP BEVIT -------------------------------

	@RequestMapping("/appBevit")
	String appBevit(HttpServletRequest request) {
		request.getSession().setAttribute("accountRole", "undefined");
		request.setAttribute("currentPage", "home");
		return "undefined-role/undefined-app";
	}

	@RequestMapping("/logout")
	String logout(HttpServletRequest request) {
		Account accountLogout = (Account) request.getSession().getAttribute("accountLog");
		if(accountLogout.getRuolo() == 'U') {
			request.getSession().removeAttribute("productsInCarrello");
			request.getSession().removeAttribute("listCarrelloByAccountId");
		}
		request.getSession().removeAttribute("accountLog");
		request.getSession().setAttribute("accountRole", "undefined");
		request.setAttribute("currentPage", "home");
		return "undefined-role/undefined-app";
	}



}
