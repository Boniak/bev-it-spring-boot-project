package org.web.controllers.undefined;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.common.entities.Account;
import org.common.entities.Contatto;
import org.common.entities.Indirizzo;
import org.common.entities.Prodotto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.web.webservices.AccountWebService;
import org.web.webservices.CarrelloWebService;
import org.web.webservices.ContattoWebService;
import org.web.webservices.IndirizzoWebService;
import org.web.webservices.ProdottoWebService;

@Controller
public class UndefinedController {

	@Autowired
	ProdottoWebService prodottoWebService;

	@Autowired
	AccountWebService accountWebService;

	@Autowired
	ContattoWebService contattoWebService;

	@Autowired
	IndirizzoWebService indirizzoWebService;

	@Autowired
	CarrelloWebService carrelloWebService;


	// ------------------------- HOME --------------------------------

	@RequestMapping("/undefined/home")
	String home(HttpServletRequest request) {
		request.setAttribute("currentPage", "home");
		return "undefined-role/undefined-app";
	}

	// ----------------------- PRODOTTI ------------------------------

	@RequestMapping("/undefined/prodotti")
	String prodotti(HttpServletRequest request) {
		// prendo la lista dei prodotti dal DB
		List<Prodotto> listaProdotti = prodottoWebService.getListProdotto();
		request.setAttribute("listaProdotti", listaProdotti);
		request.setAttribute("currentPage", "prodotti");
		return "undefined-role/undefined-app";
	}

	// ------------------------- LOGIN -------------------------------

	@RequestMapping("/undefined/pagina-login")
	String loginPage(HttpServletRequest request) {
		request.setAttribute("currentPage", "login");
		return "undefined-role/undefined-app";	
	}

	@RequestMapping("/undefined/login")
	String login(HttpServletRequest request) {

		String username = request.getParameter("username");

		String password = request.getParameter("password");

		Optional<Account> account = accountWebService.checkAccountByUsernameAndPassword(username, password);

		// check se l'account è presente con i parametri passati, se non trovato, ritorna alla pagina di login
		if(!account.isPresent() || account.get().isEnableAccount() == false) {
			String msgErr = "Username e/o Password errati !";
			request.setAttribute("messaggioErrore", msgErr);
			request.setAttribute("currentPage", "login");
			return "undefined-role/undefined-app";
		}

		// setto in sessione l'account trovato, per alcune operazioni visive
		// trasformo il ruolo in String perché il char da problemi con thymeleaf
		String ruolo = Character.toString(account.get().getRuolo());
		request.getSession().setAttribute("accountRole", ruolo);
		request.getSession().setAttribute("accountLog", account.get());

		if(ruolo.equals("A")) {
			request.setAttribute("currentPage", "home");
			return "admin-role/admin-app";
		} else {
			// prendo il numero di prodotti nel carrello dell'utente in sessione
			int numeroProdottiInCarrello = carrelloWebService.getQuantitaProdottiInCarrelloByAccountId(account.get().getId());
			request.getSession().setAttribute("productsInCarrello", numeroProdottiInCarrello);
			request.setAttribute("currentPage", "home");
			return "user-role/user-app";
		}			
	}

	// --------------------- REGISTRAZIONE --------------------------

	@RequestMapping("/undefined/pagina-registrazione")
	String paginaRegistrazione(HttpServletRequest request) {
		request.setAttribute("currentPage", "registrazione");
		return "undefined-role/undefined-app";
	}

	@RequestMapping("/undefined/registrazione")
	String registrazione(HttpServletRequest request) {

		String errorMessage = "";

		// provo ad eseguire la registrazione, se non va a buon fine,
		// reindirizzo alla pagina di registrazione con un messaggio di errore
		try {

			// indirizzo
			String stato = request.getParameter("stato");
			errorMessage = stato.equals("") ? "Stato can not be empty" : errorMessage;

			String citta = request.getParameter("citta");
			errorMessage = citta.equals("") ? "Citta' can not be empty" : errorMessage;

			String provincia = request.getParameter("provincia").toUpperCase();
			errorMessage = provincia.equals("") ? "Provincia can not be empty" : errorMessage;

			String cap = request.getParameter("cap");
			errorMessage = cap.equals("") ? "Cap can not be empty" : errorMessage;

			String via = request.getParameter("via");
			errorMessage = via.equals("") ? "Via can not be empty" : errorMessage;


			// contatto
			String nome = request.getParameter("nome");
			errorMessage = nome.equals("") ? "Nome can not be empty" : errorMessage;

			String cognome = request.getParameter("cognome");
			errorMessage = cognome.equals("") ? "Cognome can not be empty" : errorMessage;

			String email = request.getParameter("email");
			errorMessage = email.equals("") ? "Email can not be empty" : errorMessage;

			String codiceFiscale = request.getParameter("codiceFiscale");			
			errorMessage = codiceFiscale.equals("") ? "Codice Fiscale can not be empty" : errorMessage;

			String numeroDiTel = request.getParameter("numero");
			errorMessage = numeroDiTel.equals("") ? "Numero di Telefono can not be empty" : errorMessage;


			// account
			String username = request.getParameter("username");
			errorMessage = username.equals("") ? "Username can not be empty" : errorMessage;
			String password = request.getParameter("password");
			errorMessage = password.equals("") ? "Password can not be empty" : errorMessage;


			// ----------------------------- CHECKS -----------------------------------

			/**
			 * check per controllare se uno dei parametri inseriti risulta vuoto.
			 * se si, l'utente verrà reindirizzato nuovamente alla pagina di registrazione
			 * con un messaggio di errore specifico valorizzato con il valore di 'errorMessage'.
			 */
			if (!errorMessage.equals("")) {
				// messaggio di errore
				request.setAttribute("msgErrorRegistration", errorMessage);

				request.setAttribute("currentPage", "registrazione");
				return "undefined-role/undefined-app";
			}

			/**
			 * check per controllare se l'username è già presente nel DB
			 * se si, l'utente verrà reindirizzato nuovamente alla pagina di registrazione
			 * con un messaggio di errore valorizzato con il valore di 'errorMessage'.
			 */
			if(accountWebService.checkIfUsernameIsPresent(username)) {
				// messaggio di errore
				errorMessage = "Username already present!";
				request.setAttribute("msgErrorRegistration", errorMessage);
				request.setAttribute("currentPage", "registrazione");
				return "undefined-role/undefined-app";
			}


			// -------------------------- REGISTRATION --------------------------------

			Indirizzo indirizzo = indirizzoWebService
					.insertIndirizzo(new Indirizzo(0, stato, citta, provincia, cap, via));
			Contatto contatto = contattoWebService
					.insertContatto(new Contatto(0, nome, cognome, codiceFiscale, email, numeroDiTel, indirizzo));

			/*
			 * il ruolo verrà sempre impostato ad 'U' se la registrazione di un utente non
			 * viene effettuata da un Admin.
			 */
			accountWebService.insertAccount(new Account(0, username, password, 'U', contatto, true));

		} catch (Exception e) {
			// messaggio di errore
			errorMessage = "Something went wrong!";
			request.setAttribute("msgErrorRegistration", errorMessage);
			request.setAttribute("currentPage", "registrazione");
			return "undefined/undefined-app";
		}

		request.setAttribute("currentPage", "home");
		return "undefined-role/undefined-app";
	}

}
