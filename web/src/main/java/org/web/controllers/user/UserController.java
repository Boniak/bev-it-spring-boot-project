package org.web.controllers.user;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.common.entities.Account;
import org.common.entities.Carrello;
import org.common.entities.Contatto;
import org.common.entities.DettaglioFattura;
import org.common.entities.Fattura;
import org.common.entities.Indirizzo;
import org.common.entities.Prodotto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.web.webservices.AccountWebService;
import org.web.webservices.CarrelloWebService;
import org.web.webservices.ContattoWebService;
import org.web.webservices.DettaglioFatturaWebService;
import org.web.webservices.FatturaWebService;
import org.web.webservices.IndirizzoWebService;
import org.web.webservices.ProdottoWebService;

@Controller
public class UserController {

	@Autowired
	FatturaWebService fatturaWebService;

	@Autowired
	CarrelloWebService carrelloWebService;

	@Autowired
	ProdottoWebService prodottoWebService;

	@Autowired
	AccountWebService accountWebService;

	@Autowired
	DettaglioFatturaWebService dettaglioFatturaWebService;

	@Autowired
	IndirizzoWebService indirizzoWebService;

	@Autowired
	ContattoWebService contattoWebService;


	// ---------------------------- HOME ------------------------------

	@RequestMapping("/user/home")
	String home(HttpServletRequest request) {
		request.setAttribute("currentPage", "home");
		return "user-role/user-app";
	}

	// -------------------------- PRODOTTI ----------------------------

	@RequestMapping("/user/prodotti")
	String prodotti(HttpServletRequest request) {
		// prendo la lista dei prodotti dal DB
		List<Prodotto> listaProdotti = prodottoWebService.getListProdotto();
		request.setAttribute("listaProdotti", listaProdotti);
		request.setAttribute("currentPage", "prodotti");
		return "user-role/user-app";
	}	

	@RequestMapping("/user/aggiungi-al-carrello")
	String aggiungiAlCarrello(HttpServletRequest request) {

		Account accountLog = (Account) request.getSession().getAttribute("accountLog");
		int idProdottoAggiunto = Integer.parseInt(request.getParameter("idProdotto"));

		// controllo se l'utente ha gia' quel determinato prodotto nel carrello
		// se il prodotto e' presente --> aumento la sua quantita' di 1
		// se il prodotto non e' presente --> inserisco il nuovo prodotto con quantita'
		// 1.
		try {
			Carrello carrello = carrelloWebService.checkProdottoInCarrello(accountLog.getId(),
					idProdottoAggiunto);
			// metodo nella classe Carrello che aumenta la quantita' di 1
			carrello.addQuantitaByOne();
			carrelloWebService.updateCarrello(carrello);
		} catch (Exception e) {
			Prodotto prodottoAggiunto = prodottoWebService.getProdottoById(idProdottoAggiunto).get();
			carrelloWebService.insertCarrello(new Carrello(0, accountLog, prodottoAggiunto, 1));
		}

		// prendo nuovamente la lista dei prodotti dal DB
		List<Prodotto> listaProdotti = prodottoWebService.getListProdotto();
		request.setAttribute("listaProdotti", listaProdotti);

		int numeroProdottiInCarrello = (int) request.getSession().getAttribute("productsInCarrello") + 1;
		request.getSession().setAttribute("productsInCarrello", numeroProdottiInCarrello);

		request.setAttribute("currentPage", "prodotti");
		return "user-role/user-app";
	}

	// --------------------------- PROFILO ----------------------------

	@RequestMapping("/user/profilo")
	String profilo(HttpServletRequest request) {
		Account a = (Account) request.getSession().getAttribute("accountLog");

		List<Fattura> listaFatture = new ArrayList<Fattura>();
		try {
			listaFatture = fatturaWebService.getListFatturaByAccountId(a.getId());
		} catch (Exception e) {
			System.out.println("Cliente senza fatture!");
		}
		request.getSession().setAttribute("listFattureCliente", listaFatture);

		request.setAttribute("currentPage", "profilo");
		return "user-role/user-app";
	}

	@RequestMapping("/user/pagina-modifica-profilo")
	String paginaModificaProfilo(HttpServletRequest request) {
		request.setAttribute("currentPage", "modifica-profilo");
		return "user-role/user-app";
	}

	@RequestMapping("/user/modifica-profilo")
	String modificaProfilo(HttpServletRequest request) {

		String errorMessage = "";

		int idAccount = Integer.parseInt(request.getParameter("idAccount"));
		String username = request.getParameter("username");
		errorMessage = username.equals("") ? "Username can not be empty" : errorMessage;
		String password = request.getParameter("password");
		errorMessage = password.equals("") ? "Password can not be empty" : errorMessage;

		int idContatto = Integer.parseInt(request.getParameter("idContatto"));
		String nome = request.getParameter("nome");
		errorMessage = nome.equals("") ? "Nome can not be empty" : errorMessage;
		String cognome = request.getParameter("cognome");
		errorMessage = cognome.equals("") ? "Cognome can not be empty" : errorMessage;
		String email = request.getParameter("email");
		errorMessage = email.equals("") ? "Email can not be empty" : errorMessage;
		String codiceFiscale = request.getParameter("codiceFiscale");
		errorMessage = codiceFiscale.equals("") ? "Codice Fiscale can not be empty" : errorMessage;
		String numero = request.getParameter("numero");
		errorMessage = numero.equals("") ? "Numero can not be empty" : errorMessage;

		int idIndirizzo = Integer.parseInt(request.getParameter("idIndirizzo"));
		String stato = request.getParameter("stato");
		errorMessage = stato.equals("") ? "Stato can not be empty" : errorMessage;
		String citta = request.getParameter("citta");
		errorMessage = citta.equals("") ? "Citta can not be empty" : errorMessage;
		String provincia = request.getParameter("provincia");
		errorMessage = provincia.equals("") ? "Provincia can not be empty" : errorMessage;
		String cap = request.getParameter("cap");
		errorMessage = cap.equals("") ? "Cap can not be empty" : errorMessage;
		String via = request.getParameter("via");
		errorMessage = via.equals("") ? "Via can not be empty" : errorMessage;

		if (!errorMessage.equals("")) {
			// messaggio di errore
			request.setAttribute("msgUpdateError", errorMessage);
			request.setAttribute("currentPage", "modifica-profilo");
			return "user-role/user-app";
		}

		Indirizzo indirizzo = indirizzoWebService.updateIndirizzo(new Indirizzo(idIndirizzo,stato,citta,provincia,cap,via));
		Contatto contatto = contattoWebService.updateContatto(new Contatto(idContatto,nome,cognome,codiceFiscale,email,numero,indirizzo));
		try {
			Account account = accountWebService.updateAccount(new Account(idAccount,username,password,'U',contatto,true));
			request.getSession().setAttribute("accountLog", account);
			request.setAttribute("currentPage", "profilo");
			return "user-role/user-app";
		} catch (Exception e) {
			request.setAttribute("msgUpdateError", "username gia' presente!");
			request.setAttribute("currentPage", "modifica-profilo");
			return "user-role/user-app";
		}	
	}

	@RequestMapping("/user/elimina-account")
	String eliminaProfilo(HttpServletRequest request) {
		int idAccount = Integer.parseInt(request.getParameter("idAccount"));
		accountWebService.disableAccount(idAccount);
		request.getSession().removeAttribute("productsInCarrello");
		request.getSession().removeAttribute("listCarrelloByAccountId");
		request.getSession().removeAttribute("accountLog");
		request.getSession().setAttribute("accountRole", "undefined");
		request.setAttribute("currentPage", "home");
		return "undefined-role/undefined-app";
	}

	@RequestMapping("/user/dettagli-fattura")
	String dettagliFattura(HttpServletRequest request) {
		int idFattura = Integer.parseInt(request.getParameter("idFattura"));
		List<DettaglioFattura> listaDettagliFattura = dettaglioFatturaWebService.getListDettagliFatturaByFatturaId(idFattura);
		double totalPrice = 0.0;
		for (DettaglioFattura dettaglioFattura : listaDettagliFattura) {
			totalPrice += dettaglioFattura.getPrezzoUnitario() * dettaglioFattura.getQuantita();
		}
		request.setAttribute("listaDettagliFattura", listaDettagliFattura);
		request.setAttribute("totalDettagliPrice", totalPrice);

		request.setAttribute("currentPage", "dettagli-fattura");
		return "user-role/user-app";
	}

	// --------------------------- CARRELLO ----------------------------

	@RequestMapping("/user/carrello")
	String carrello(HttpServletRequest request) {
		int idAccount = ((Account) request.getSession().getAttribute("accountLog")).getId();
		List<Carrello> listaCarrello = carrelloWebService.getListCarrelloByAccountId(idAccount);

		request.getSession().setAttribute("listCarrelloByAccountId", listaCarrello);

		calcolaTotaleCarrello(listaCarrello, request);

		request.setAttribute("currentPage", "carrello");
		return "user-role/user-app";
	}

	@RequestMapping("/user/aumenta-quantita-carrello")
	String aumentaQuantitaCarrello(HttpServletRequest request) {
		int idCarrello = Integer.parseInt(request.getParameter("idCarrello"));
		@SuppressWarnings("unchecked")
		List<Carrello> listaCarrello = (List<Carrello>) request.getSession().getAttribute("listCarrelloByAccountId");

		listaCarrello.stream().filter(c -> c.getId() == idCarrello).forEach(c -> {
			c.addQuantitaByOne();
			carrelloWebService.updateCarrello(c);
		});

		calcolaTotaleCarrello(listaCarrello, request);

		request.getSession().setAttribute("listCarrelloByAccountId", listaCarrello);

		int numeroProdottiInCarrello = (int) request.getSession().getAttribute("productsInCarrello") + 1;
		request.getSession().setAttribute("productsInCarrello", numeroProdottiInCarrello);

		request.setAttribute("currentPage", "carrello");
		return "user-role/user-app";
	}

	@RequestMapping("/user/diminuisci-quantita-carrello")
	String diminuisciQuantitaCarrello(HttpServletRequest request) {
		int idCarrello = Integer.parseInt(request.getParameter("idCarrello"));
		@SuppressWarnings("unchecked")
		List<Carrello> listaCarrello = (List<Carrello>) request.getSession().getAttribute("listCarrelloByAccountId");

		listaCarrello.stream().filter(c -> c.getId() == idCarrello).forEach(c -> {
			c.removeQuantitaByOne();
			carrelloWebService.updateCarrello(c);
		});

		calcolaTotaleCarrello(listaCarrello, request);

		request.getSession().setAttribute("listCarrelloByAccountId", listaCarrello);

		int numeroProdottiInCarrello = (int) request.getSession().getAttribute("productsInCarrello") - 1;
		request.getSession().setAttribute("productsInCarrello", numeroProdottiInCarrello);

		request.setAttribute("currentPage", "carrello");
		return "user-role/user-app";
	}

	@RequestMapping("/user/rimuovi-dal-carrello")
	String rimuoviDalCarrello(HttpServletRequest request) {
		int idCarrello = Integer.parseInt(request.getParameter("idCarrello"));
		@SuppressWarnings("unchecked")
		List<Carrello> listaCarrello = (List<Carrello>) request.getSession().getAttribute("listCarrelloByAccountId");

		int numProdottiDaRimuovere = (int) listaCarrello.stream().filter(c -> c.getId() == idCarrello).map(c -> c.getQuantita()).findAny().get();

		// rimozione del carrello sia dal DB che dalla lista in sessione.
		listaCarrello.stream().filter(c -> c.getId() == idCarrello).forEach(c -> carrelloWebService.deleteCarrello(c));	
		listaCarrello.removeIf(c -> c.getId() == idCarrello);

		calcolaTotaleCarrello(listaCarrello, request);

		request.getSession().setAttribute("listCarrelloByAccountId", listaCarrello);

		int numeroProdottiInCarrello = (int) request.getSession().getAttribute("productsInCarrello") - numProdottiDaRimuovere;
		request.getSession().setAttribute("productsInCarrello", numeroProdottiInCarrello);

		request.setAttribute("currentPage", "carrello");
		return "user-role/user-app";
	}

	// metodo per calcolare il prezzo totale dei prodotti in carrello

	private void calcolaTotaleCarrello(List<Carrello> listaCarrello, HttpServletRequest request) {
		double prezzoTotale = 0.0;
		for (Carrello carrello : listaCarrello) {
			prezzoTotale += carrello.getIdProdotto().getPrezzo() * carrello.getQuantita();
		}
		request.setAttribute("totalPrice", prezzoTotale);
	}

	@RequestMapping("/user/ordina-prodotti")
	String ordinaProdotti(HttpServletRequest request) {

		Account accountLog = (Account) request.getSession().getAttribute("accountLog");

		/*
		 *  prendo il numero fattura più alto dal DB.
		 *  - se è uguale a 0 (solo per la prima fattura), la variabile 'numeroFattura' sarà impostata a 1.
		 *  - se è diverso da 0 la variabile 'numeroFattura' prenderà il valore di 'numeroFatturaMax' + 1.
		 */
		int numeroFattura = fatturaWebService.selectMaxNumeroFattura() + 1;

		List<Carrello> listaCarrello = carrelloWebService.getListCarrelloByAccountId(accountLog.getId());

		// preparo una variabile per poter ospitare il totale della fattura, calcolato di seguito.
		double totale = 0.0;

		for (Carrello carrello : listaCarrello) {			
			totale += carrello.getIdProdotto().getPrezzo() * carrello.getQuantita();
		}

		// prendo la data attuale, mi servirà per impostare 'anno' e 'data' dell'oggetto fattura.
		LocalDate dataFattura = LocalDate.now();

		Fattura fatturaOrdine = fatturaWebService.insertFattura(
				new Fattura(0, numeroFattura, totale, Date.valueOf(dataFattura), accountLog));


		listaCarrello.stream().forEach(carrello -> {
			dettaglioFatturaWebService.insertDettaglioFattura(
					new DettaglioFattura(0,fatturaOrdine,carrello.getIdProdotto(),carrello.getQuantita(),
							carrello.getIdProdotto().getPrezzo(),carrello.getIdProdotto().getAliquotaIva()));
			carrelloWebService.deleteCarrello(carrello);
		});

		request.getSession().setAttribute("listCarrelloByAccountId", null);
		request.getSession().setAttribute("productsInCarrello", 0);
		request.setAttribute("totalPrice", 0);
		request.setAttribute("orderPlaced", "Done!");

		request.setAttribute("currentPage", "carrello");
		return "user-role/user-app";
	}
}