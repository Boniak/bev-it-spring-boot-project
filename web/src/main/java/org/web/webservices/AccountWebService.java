package org.web.webservices;

import org.common.controllers.AccountController;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;

@FeignClient(name = "accountWebService", url = "http://localhost:8083/accounts")
@Service
public interface AccountWebService extends AccountController {

}
