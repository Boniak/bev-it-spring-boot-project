package org.web.webservices;

import org.common.controllers.CarrelloController;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;

@FeignClient(name = "carrelloWebService", url = "http://localhost:8083/carrelli")
@Service
public interface CarrelloWebService extends CarrelloController {

}
