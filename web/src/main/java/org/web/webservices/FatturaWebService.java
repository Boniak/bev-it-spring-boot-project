package org.web.webservices;

import org.common.controllers.FatturaController;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;

@FeignClient(name = "fatturaWebService", url = "http://localhost:8083/fatture")
@Service
public interface FatturaWebService extends FatturaController {

}
