package org.web.webservices;

import org.common.controllers.IndirizzoController;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;

@FeignClient(name = "indirizzoWebService", url = "http://localhost:8083/indirizzi")
@Service
public interface IndirizzoWebService extends IndirizzoController {

}
