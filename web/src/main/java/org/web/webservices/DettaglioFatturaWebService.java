package org.web.webservices;

import org.common.controllers.DettaglioFatturaController;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;

@FeignClient(name = "dettaglioFatturaWebService", url = "http://localhost:8083/dettagli_fattura")
@Service
public interface DettaglioFatturaWebService extends DettaglioFatturaController {

}
