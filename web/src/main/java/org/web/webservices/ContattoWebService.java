package org.web.webservices;

import org.common.controllers.ContattoController;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;

@FeignClient(name = "contattoWebService", url = "http://localhost:8083/contatti")
@Service
public interface ContattoWebService extends ContattoController {

}
