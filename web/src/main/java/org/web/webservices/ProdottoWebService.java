package org.web.webservices;

import org.common.controllers.ProdottoController;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;

@FeignClient(name = "prodottoWebService", url = "http://localhost:8083/prodotti")
@Service
public interface ProdottoWebService extends ProdottoController {

}
