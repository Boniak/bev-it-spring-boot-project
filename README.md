# DESCRIZIONE PROGETTO
<b>
Bev-it e' un sito di compravendita di bevande. <br>
Gli utenti possono accedere al sito con 3 diversi ruoli: Admin, User, Undefined. <br>
A seconda del ruolo con cui l'utente fara' accesso, avra' a disposizione diverse funzionalita'. <br>
</b>

# DESCRIZIONE FUNZIONI PER RUOLO: 

<b style="color: red;"> RUOLO "UNDEFINED" : </b> <br>

- <b> visualizzazione della home page del locale. </b> <br> <br>

- <b> consultazione prodotti. </b> <br>
    @GetMapping("--/prodotti/list") List<Prodotto> getListProdotto(); <br> <br>

- <b> accesso alla pagina di registrazione. </b> <br>
    @PostMapping("--/accounts/insert") Account insertAccount(@RequestBody Account account); <br> <br>

- <b> accesso alla pagina di login. </b> <br>
    @GetMapping("--/accounts/login-check") Optional<Account> checkAccountByUsernameAndPassword(@RequestParam String username, @RequestParam String password); <br> <br>

# -----------------------------------------------------------------------------------------

<b style="color: red;"> RUOLO "USER" : </b> <br>

- <b> visualizzazione della home page del locale. </b> <br> <br>

- <b> consultazione prodotti. </b> <br>
    @GetMapping("--/prodotti/list") List<Prodotto> getListProdotto(); <br> <br>

- <b> possibilita' di aggiungere prodotti al carrello, se il prodotto e' gia' presente nel carrello ne aumenta la quantita'. </b> <br>
    @PostMapping("--/carrelli/insert") Carrello insertCarrello(@RequestBody Carrello carrello); <br>
    @PutMapping("--/carrelli/update") Carrello updateCarrello(@RequestBody Carrello carrello); <br> <br>

- <b> accesso alla pagina profilo, dove sono riportati i dati del proprio account e la lista degli ordini effettuati(con relativi dettagli-ordine). </b> <br>
    @GetMapping("--/fatture/{idAccount}") List<Fattura> getListFatturaByAccountId(@PathVariable(name = "idAccount")int idAccount); <br>
    @GetMapping("--/dettagli-fattura/{idFattura}") List<DettaglioFattura> getListDettagliFatturaByFatturaId(@PathVariable(name = "idFattura") int idFattura); <br> <br>

- <b> possibilita' di modificare o disabilitare il proprio account. </b> <br>
    @PutMapping("--/accounts/disable/{idAccount}") void disableAccount(@PathVariable(name = "idAccount") int idAccount); <br>
    @PutMapping("--/accounts/update") Account updateAccount(@RequestBody Account account); <br> <br>
    
- <b> accesso alla pagina carrello, qui sono riportati gli articoli aggiunti ad esso dall'utente, con possibilita' di cambiare la quantita' e calcolo del totale. </b> <br>
    @GetMapping("--/carrelli/list/{idAccount}") List<Carrello> getListCarrelloByAccountId(@PathVariable(name = "idAccount") int idAccount); <br>
    @PutMapping("--/carrelli/update") Carrello updateCarrello(@RequestBody Carrello carrello); <br> <br>

- <b> possibilita' di ordinare i prodotti, generando la relativa fattura e dettagli-ordine. </b> <br>
    @PostMapping("--/fatture/insert") Fattura insertFattura(@RequestBody Fattura fattura); <br>
    @PostMapping("--/dettagli-fattura/insert") DettaglioFattura insertDettaglioFattura(@RequestBody DettaglioFattura dettaglioFattura); <br> <br>

- <b> logout. </b> <br>
    Comprende la rimozione degli attributi in sessione come : utente, liste prodotti e carrello. <br> <br>

# -----------------------------------------------------------------------------------------

<b style="color: red;"> RUOLO "ADMIN" : </b> <br>

- <b> visualizzazione della home page del locale. </b> <br> <br>

- <b> possibilita' di inserire, modificare, e disabilitare i prodotti. </b> <br>
    @PostMapping("--/prodotti/insert") Prodotto insertProdotto(@RequestBody Prodotto indirizzo); <br>
	@PutMapping("--/prodotti/update") Prodotto updateProdotto(@RequestBody Prodotto indirizzo); <br>
    @PutMapping("--/prodotti/disable/{idProdotto}") void disableProdotto(@PathVariable(name = "idProdotto")int idProdotto); <br> <br>

- <b> accesso alla pagina ordini, dove sono riportate le fatture emesse in ordine cronologico inverso, con relativi dettagli-ordine. </b> <br>
    @GetMapping("--/fatture/list") List<Fattura> getListFattura(); <br>
    @GetMapping("--/dettagli-fattura/lista-dettagli/{idFattura}") List<DettaglioFattura> getListDettagliFatturaByFatturaId(@PathVariable(name = "idFattura") int idFattura); <br> <br>

- <b> accesso alla pagina utenti, contenente la lista degli utenti registrati, con possibilita' di filtraggio tramite nome e cognome. </b> <br>
    @GetMapping("--/accounts/list") List<Account> getListAccount(); <br>
    @GetMapping("--/accounts/list/nome-cognome") List<Account> getListAccountByNomeAndCognome(@RequestParam String nome, @RequestParam String cognome); <br> <br>

- <b> accesso alla pagina profilo, dove sono riportati i dati del proprio account ed presente una funzione che permette di inserire un utente con ruolo a scelta. </b> <br>
    @PostMapping("--/accounts/insert") Account insertAccount(@RequestBody Account account); <br> <br>

- <b> possibilita' di modificare o disabilitare il proprio account (solo se non è l'ultimo utente con ruolo 'Admin'). </b> <br>
    @PutMapping("--/accounts/disable/{idAccount}") void disableAccount(@PathVariable(name = "idAccount") int idAccount); <br>
    @PutMapping("--/accounts/update") Account updateAccount(@RequestBody Account account); <br> <br>

- <b> logout. </b> <br>
    Comprende la rimozione degli attributi in sessione come : utente, liste prodotti e carrello. <br> <br>
