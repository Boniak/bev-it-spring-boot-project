package org.service.services;

import java.util.List;
import java.util.Optional;

import org.common.controllers.ContattoController;
import org.common.entities.Contatto;
import org.service.repositories.ContattoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/contatti")
public class ContattoService implements ContattoController{

	@Autowired
	ContattoRepository contattoRepository;
	
	@Override
	public List<Contatto> getListContatto() {
		return contattoRepository.findAll();
	}

	@Override
	public Optional<Contatto> getContattoById(int id) {
		return contattoRepository.findById(id);
	}

	@Override
	public Contatto insertContatto(Contatto contatto) {
		return contattoRepository.save(contatto);
	}

	@Override
	public Contatto updateContatto(Contatto contatto) {
		return contattoRepository.save(contatto);
	}

	@Override
	public void deleteContatto(Contatto contatto) {
		contattoRepository.delete(contatto);
	}

}
