package org.service.services;

import java.util.List;
import java.util.Optional;

import org.common.controllers.CarrelloController;
import org.common.entities.Carrello;
import org.service.repositories.CarrelloRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/carrelli")
public class CarrelloService implements CarrelloController{

	@Autowired
	CarrelloRepository carrelloRepository;
	
	@Override
	public List<Carrello> getListCarrello() {
		return carrelloRepository.findAll();
	}

	@Override
	public Optional<Carrello> getCarrelloById(int id) {
		return carrelloRepository.findById(id);
	}

	@Override
	public Carrello insertCarrello(Carrello carrello) {
		return carrelloRepository.save(carrello);
	}

	@Override
	public Carrello updateCarrello(Carrello carrello) {
		return carrelloRepository.save(carrello);
	}

	@Override
	public void deleteCarrello(Carrello carrello) {
		carrelloRepository.delete(carrello);
	}

	@Override
	public Carrello checkProdottoInCarrello(int idAccount, int idProdotto) {
		return carrelloRepository.checkProdottoInCarrello(idAccount, idProdotto);
	}

	@Override
	public List<Carrello> getListCarrelloByAccountId(int idAccount) {
		return carrelloRepository.getListCarrelloByAccountId(idAccount);
	}

	@Override
	public void deleteCarrelloByUtenteId(int idAccount) {
		carrelloRepository.deleteCarrelloByUtenteId(idAccount);
	}

	@Override
	public int getQuantitaProdottiInCarrelloByAccountId(int idAccount) {
		/* 
		 * assegno alla seguente variabile il totale della lista di Integer fornita dal DB,
		 * la lista contiene la quantità dei prodotti nel carrello dell'utente in sessione.
		 */
		int numeroProdottiInCarrello = 0;
		for (Integer numero : carrelloRepository.getQuantitaProdottiInCarrelloByAccountId(idAccount)) {
			numeroProdottiInCarrello += numero.intValue();
		} 
		return numeroProdottiInCarrello;
	}

}
