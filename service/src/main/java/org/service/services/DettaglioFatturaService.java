package org.service.services;

import java.util.List;
import java.util.Optional;

import org.common.controllers.DettaglioFatturaController;
import org.common.entities.DettaglioFattura;
import org.service.repositories.DettaglioFatturaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/dettagli_fattura")
public class DettaglioFatturaService implements DettaglioFatturaController{

	@Autowired
	DettaglioFatturaRepository dettaglioFatturaRepository;
	
	@Override
	public List<DettaglioFattura> getListDettaglioFattura() {
		return dettaglioFatturaRepository.findAll();
	}

	@Override
	public Optional<DettaglioFattura> getDettaglioFatturaById(int id) {
		return dettaglioFatturaRepository.findById(id);
	}

	@Override
	public DettaglioFattura insertDettaglioFattura(DettaglioFattura dettaglioFattura) {
		return dettaglioFatturaRepository.save(dettaglioFattura);
	}

	@Override
	public DettaglioFattura updateDettaglioFattura(DettaglioFattura dettaglioFattura) {
		return dettaglioFatturaRepository.save(dettaglioFattura);
	}

	@Override
	public void deleteDettaglioFattura(DettaglioFattura dettaglioFattura) {
		dettaglioFatturaRepository.delete(dettaglioFattura);
	}

	@Override
	public List<DettaglioFattura> getListDettagliFatturaByFatturaId(int idFattura) {
		return dettaglioFatturaRepository.getListDettagliFatturaByFatturaId(idFattura);
	}

}
