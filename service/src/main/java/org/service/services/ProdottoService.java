package org.service.services;

import java.util.List;
import java.util.Optional;

import org.common.controllers.ProdottoController;
import org.common.entities.Prodotto;
import org.service.repositories.ProdottoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/prodotti")
public class ProdottoService implements ProdottoController{

	@Autowired
	ProdottoRepository prodottoRepository;
	
	@Override
	public List<Prodotto> getListProdotto() {
		return prodottoRepository.getListOfEnableProdotto();
	}

	@Override
	public Optional<Prodotto> getProdottoById(int id) {
		return prodottoRepository.findById(id);
	}

	@Override
	public Prodotto insertProdotto(Prodotto prodotto) {
		return prodottoRepository.save(prodotto);
	}

	@Override
	public Prodotto updateProdotto(Prodotto prodotto) {
		return prodottoRepository.save(prodotto);
	}

	@Override
	public void disableProdotto(int idProdotto) {
	    prodottoRepository.disableProdotto(idProdotto);
	}
	
	@Override
	public void deleteProdotto(Prodotto prodotto) {
		prodottoRepository.delete(prodotto);
	}

}
