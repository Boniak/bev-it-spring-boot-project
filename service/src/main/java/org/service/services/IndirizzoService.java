package org.service.services;

import java.util.List;
import java.util.Optional;

import org.common.controllers.IndirizzoController;
import org.common.entities.Indirizzo;
import org.service.repositories.IndirizzoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/indirizzi")
public class IndirizzoService implements IndirizzoController{

	@Autowired
	IndirizzoRepository indirizzoRepository;

	@Override
	public List<Indirizzo> getListIndirizzo() {
		return indirizzoRepository.findAll();
	}

	@Override
	public Optional<Indirizzo> getIndirizzoById(int id) {
		return indirizzoRepository.findById(id);
	}

	@Override
	public Indirizzo insertIndirizzo(Indirizzo indirizzo) {
		return indirizzoRepository.save(indirizzo);
	}

	@Override
	public Indirizzo updateIndirizzo(Indirizzo indirizzo) {
		return indirizzoRepository.save(indirizzo);
	}

	@Override
	public void deleteIndirizzo(Indirizzo indirizzo) {
		indirizzoRepository.delete(indirizzo);
	}

}
