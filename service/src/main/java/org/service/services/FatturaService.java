package org.service.services;

import java.util.List;
import java.util.Optional;

import org.common.controllers.FatturaController;
import org.common.entities.Fattura;
import org.service.repositories.FatturaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/fatture")
public class FatturaService implements FatturaController{

	@Autowired
	FatturaRepository fatturaRepository;
	
	@Override
	public List<Fattura> getListFattura() {
		return fatturaRepository.findAll();
	}

	@Override
	public Optional<Fattura> getFatturaById(int id) {
		return fatturaRepository.findById(id);
	}

	@Override
	public Fattura insertFattura(Fattura fattura) {
		return fatturaRepository.save(fattura);
	}

	@Override
	public Fattura updateFattura(Fattura fattura) {
		return fatturaRepository.save(fattura);
	}

	@Override
	public void deleteFattura(Fattura fattura) {
		fatturaRepository.delete(fattura);
	}

	@Override
	public List<Fattura> getListFatturaByAccountId(int idAccount) {
		return fatturaRepository.getListFatturaByAccountId(idAccount);
	}

	@Override
	public int selectMaxNumeroFattura() {
		Integer numeroFattura = fatturaRepository.selectMaxNumeroFattura();
		return numeroFattura != null ? numeroFattura : 0;
	}

}
