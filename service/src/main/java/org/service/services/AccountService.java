package org.service.services;

import java.util.List;
import java.util.Optional;

import org.common.controllers.AccountController;
import org.common.entities.Account;
import org.service.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/accounts")
public class AccountService implements AccountController{

	@Autowired
	AccountRepository accountRepository;

	@Override
	public List<Account> getListAccount() {
		return accountRepository.findAll();
	}

	@Override
	public Optional<Account> getAccountById(int id) {
		return accountRepository.findById(id);
	}

	@Override
	public Account insertAccount(Account account) {
		return accountRepository.save(account);
	}

	@Override
	public Account updateAccount(Account account) {
		return accountRepository.save(account);
	}

	@Override
	public void deleteAccount(Account account) {
		accountRepository.delete(account);		
	}

	@Override
	public Optional<Account> checkAccountByUsernameAndPassword(String username, String password) {
		return accountRepository.checkAccountByUsernameAndPassword(username, password);
	}

	@Override
	public List<Account> getListAccountByRuolo(char ruolo) {
		return accountRepository.getListAccountByRuolo(ruolo);
	}

	@Override
	public boolean checkIfUsernameIsPresent(String username) {
		String usernameTrovato = accountRepository.checkIfUsernameIsPresent(username);
		return usernameTrovato!=null && !usernameTrovato.isEmpty(); 
	}

	@Override
	public void disableAccount(int idAccount) {
		accountRepository.disableAccount(idAccount);
	}

	@Override
	public List<Account> getListAccountByNomeAndCognome(String nome, String cognome) {
		return accountRepository.getListAccountByNomeAndCognome(nome, cognome);
	}

	@Override
	public String getNomeByAccountId(int idAccount) {
		return accountRepository.getNomeByAccountId(idAccount);
	}

	@Override
	public String getCognomeByAccountId(int idAccount) {
		return accountRepository.getCognomeByAccountId(idAccount);
	}

}
