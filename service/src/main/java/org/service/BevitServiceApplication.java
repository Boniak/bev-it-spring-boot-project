package org.service;

import org.service.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

/**
 * author Boni Luca
 */
@SpringBootApplication
@EntityScan("org.common.entities")
public class BevitServiceApplication implements CommandLineRunner{
	
	@Autowired
	AccountService accountService;
	
    public static void main(String[] args) {
		SpringApplication.run(BevitServiceApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
	}
}