package org.service.repositories;

import java.util.List;

import org.common.entities.Prodotto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ProdottoRepository extends JpaRepository<Prodotto, Integer>{
	
	@Transactional
	@Modifying
	@Query("UPDATE Prodotto p SET p.enableProdotto = false WHERE p.id = :idProdotto")
	void disableProdotto(int idProdotto);
	
	@Query("SELECT p FROM Prodotto p WHERE p.enableProdotto = true")
	List<Prodotto> getListOfEnableProdotto();
	
}
