package org.service.repositories;

import java.util.List;

import org.common.entities.Carrello;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CarrelloRepository extends JpaRepository<Carrello, Integer>{

	@Query("SELECT c FROM Carrello c WHERE c.idAccount.id = :idAccount AND c.idProdotto.id = :idProdotto")
	Carrello checkProdottoInCarrello(int idAccount,int idProdotto);
	
	@Query("SELECT c FROM Carrello c WHERE c.idAccount.id = :idAccount")
	List<Carrello> getListCarrelloByAccountId(int idAccount);
	
	@Query("DELETE FROM Carrello c WHERE c.idAccount.id = :idAccount")
	void deleteCarrelloByUtenteId(int idAccount);
	
	@Query("SELECT c.quantita FROM Carrello c WHERE c.idAccount.id = :idAccount")
	List<Integer> getQuantitaProdottiInCarrelloByAccountId(int idAccount);
	
}
