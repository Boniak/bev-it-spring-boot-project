package org.service.repositories;

import java.util.List;

import org.common.entities.DettaglioFattura;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DettaglioFatturaRepository extends JpaRepository<DettaglioFattura, Integer>{

	@Query("SELECT df FROM DettaglioFattura df WHERE df.idFattura.id = :idFattura")
	List<DettaglioFattura> getListDettagliFatturaByFatturaId(int idFattura);
	
}
