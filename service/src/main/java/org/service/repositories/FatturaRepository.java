package org.service.repositories;

import java.util.List;

import org.common.entities.Fattura;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface FatturaRepository extends JpaRepository<Fattura, Integer>{

	@Query("SELECT f FROM Fattura f WHERE f.idAccount.id = :idAccount")
	List<Fattura> getListFatturaByAccountId(int idAccount);
	
	@Query("SELECT max(f.numero) FROM Fattura f")
	Integer selectMaxNumeroFattura();
	
}
