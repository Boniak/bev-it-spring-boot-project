package org.service.repositories;

import java.util.List;
import java.util.Optional;

import org.common.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer>{

	@Query("SELECT a FROM Account a WHERE a.username = :username AND a.password = :password")
	Optional<Account> checkAccountByUsernameAndPassword(String username, String password);

	@Query("SELECT a FROM Account a WHERE a.ruolo = :ruolo")
	List<Account> getListAccountByRuolo(char ruolo);

	@Query("SELECT a.username FROM Account a WHERE a.username = :username")
	String checkIfUsernameIsPresent(String username);

	@Transactional
	@Modifying
	@Query("UPDATE Account a SET a.enableAccount = false WHERE a.id = :idAccount")
	void disableAccount(int idAccount);

	@Query("SELECT a FROM Account a WHERE a.idContatto.nome LIKE %:nome% AND a.idContatto.cognome LIKE %:cognome%")
	List<Account> getListAccountByNomeAndCognome(String nome, String cognome);

	@Query("SELECT a.idContatto.nome FROM Account a WHERE a.id = :idAccount")
	String getNomeByAccountId(int idAccount);

	@Query("SELECT a.idContatto.cognome FROM Account a WHERE a.id = :idAccount")
	String getCognomeByAccountId(int idAccount);

}
